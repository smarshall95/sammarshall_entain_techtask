import React from "react";
import "./App.css";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      raceData: [],
      sortedRaces: [],
      unsortedRaces: [],
      selectedCategory: null,
      time: Date.now(),
      rowsToFetch: 5
    };

    document.title = "Entain Coding Test";
  }

  // Use fetch API to gather the 10 next races to jump and add it to app state
  // then set interval to update time in app state
  componentDidMount() {
    this.getRacingData();

    this.interval = setInterval(() => {
      this.setState({ time: Date.now() });
      this.getRacingData();
    }, 1000);
  }

  // Clear the interval once app is closed/unmounted
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  // Fetch a specified number of next races to jump, sort by time ascending
  // race categories and races >=60 seconds over start time
  getRacingData = () => {
    fetch(
      `https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=${this.state.rowsToFetch}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    )
      .then(res => res.json())
      .then(json => {
        const { data } = json;

        this.setState({
          sortedRaces: []
        });

        let newRaces = [];

        const races = data.race_summaries;

        for (const [key] of Object.entries(races)) {
          const race = races[key];

          newRaces = newRaces.concat({
            advertisedStart: race.advertised_start,
            meetingName: race.meeting_name,
            raceNumber: race.race_number,
            raceName: race.race_name,
            category: race.category_id
            // TODO: You will have to work with the API payload to determine what data you require
          });
        }

        // Sort races by time to jump and add them to race list
        newRaces.sort((item1, item2) => {
          return item1.advertisedStart.seconds - item2.advertisedStart.seconds;
        });

        // Filter races for only those which are <60 seconds over start time
        let sortedRaceCount = 0;

        this.setState({
          unsortedRaces: newRaces,
          sortedRaces: newRaces.filter(value => {
            if (this.state.selectedCategory === 1) {
              if (
                sortedRaceCount < 5 &&
                value.advertisedStart.seconds - this.state.time / 1000 > -60 &&
                value.category === "9daef0d7-bf3c-4f50-921d-8e818c60fe61"
              ) {
                sortedRaceCount++;
                value.category = "Greyhounds";
                return true;
              }
            }
            if (this.state.selectedCategory === 2) {
              if (
                sortedRaceCount < 5 &&
                value.advertisedStart.seconds - this.state.time / 1000 > -60 &&
                value.category === "161d9be2-e909-4326-8c2c-35ed71fb460b"
              ) {
                sortedRaceCount++;
                value.category = "Harness";
                return true;
              }
            }
            if (this.state.selectedCategory === 3) {
              if (
                sortedRaceCount < 5 &&
                value.advertisedStart.seconds - this.state.time / 1000 > -60 &&
                value.category === "4a2788f8-e825-4d36-9894-efd4baf1cfae"
              ) {
                sortedRaceCount++;
                value.category = "Thoroughbreds";
                return true;
              }
            }
            if (this.state.selectedCategory == null) {
              if (
                sortedRaceCount < 5 &&
                value.advertisedStart.seconds - this.state.time / 1000 > -60
              ) {
                sortedRaceCount++;
                if (value.category === "4a2788f8-e825-4d36-9894-efd4baf1cfae") {
                  value.category = "Thoroughbreds";
                }
                if (value.category === "161d9be2-e909-4326-8c2c-35ed71fb460b") {
                  value.category = "Harness";
                }
                if (value.category === "9daef0d7-bf3c-4f50-921d-8e818c60fe61") {
                  value.category = "Greyhounds";
                }
                return true;
              }
            }
            return false;
          })
        });

        if (sortedRaceCount < 5) {
          this.setState({ rowsToFetch: this.state.rowsToFetch + 1 });
          this.getRacingData();
        }
      });
  };

  // Format time in XXmin XXs
  getFormattedTime = rawTime => {
    let timeMins;
    let timeSecs;
    let time;
    const timeMs = Math.round(rawTime.seconds - this.state.time / 1000);
    if (timeMs > 0) {
      timeMins = Math.round(Math.abs(timeMs) / 60);
      timeSecs = Math.abs(timeMs) % 60;
      time = "Jumps in " + timeMins + " min " + timeSecs + " seconds ";
    } else {
      timeMins = Math.round(Math.abs(timeMs) / 60);
      timeSecs = Math.abs(timeMs) % 60;
      time = "Jumped 0 min " + timeSecs + " seconds ago";
    }
    // if(timeMs<0){
    //
    // }
    // if(timeMs>0){
    //   const time = "Jumps in "+ timeMins +" min "+ timeSecs + " seconds" + " Ago";
    // }

    return time;
  };

  // Render components
  render() {
    return (
      <div className="container">
        <div className="buttonContainer">
          <button
            className="buttonToggle"
            onClick={() => this.setState({ selectedCategory: null })}
          >
            All Races
          </button>
        </div>
        <div className="categories">
          <div className="buttonContainer">
            <button
              className="buttonToggle"
              onClick={() => this.setState({ selectedCategory: 1 })}
            >
              Greyhounds
            </button>
          </div>
          <div className="buttonContainer">
            <button
              className="buttonToggle"
              onClick={() => this.setState({ selectedCategory: 2 })}
            >
              Harness
            </button>
          </div>
          <div className="buttonContainer">
            <button
              className="buttonToggle"
              onClick={() => this.setState({ selectedCategory: 3 })}
            >
              Thoroughbreds
            </button>
          </div>
        </div>

        <div className="list">
          {this.state.sortedRaces.map(item => (
            <ul>
              <span className="item">
                Race {item.raceNumber} @ {item.meetingName} -{" "}
                {this.getFormattedTime(item.advertisedStart)}
              </span>
              <p> Race Name: {item.raceName} </p>
              <p> Category: {item.category} </p>
            </ul>
          ))}
        </div>
      </div>
    );
  }
}
